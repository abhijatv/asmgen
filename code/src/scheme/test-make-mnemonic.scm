;; Simple tests
(display "EXECUTING: (define m (make-mnemonic))")
(define m (make-mnemonic))
(newline)
(display "EXECUTING: ((m 'mnemonic-print-self))")
(newline)
((m 'mnemonic-print-self))
(display "EXECUTING: ((m 'mnemonic-init))")
((m 'mnemonic-init))
(newline)
(display "EXECUTING: ((m 'mnemonic-print-self))")
(newline)
((m 'mnemonic-print-self))
(display "EXECUTING: ((m 'mnemonic-set) \"ADDD\")")
((m 'mnemonic-set) "ADDD")
(newline)
(display "EXECUTING: ((m 'mnemonic-print-self))")
(newline)
((m 'mnemonic-print-self))
(display "EXECUTING: ((m 'mnemonic-check))")
((m 'mnemonic-check))
(newline)
(display "EXECUTING: ((m 'mnemonic-print-self))")
(newline)
((m 'mnemonic-print-self))
(display "EXECUTING: ((m 'mnemonic-get))")
(newline)
(display ((m 'mnemonic-get)))
(newline)
(display "EXECUTING: ((m 'mnemonic-print-self))")
(newline)
((m 'mnemonic-print-self))
(display "EXECUTING: ((m 'mnemonic-get-insn))")
(newline)
(display ((m 'mnemonic-get-insn)))
(newline)
(display "DONE.")
(newline)
